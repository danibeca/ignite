package com.sprhib.ui.page;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PageObject {

	protected static String URL_BASE = "http://localhost:8080/spr-mvc-hib";
	protected WebDriver driver;

	public PageObject(WebDriver driver) {
		this.driver = driver;
	}

	public String getTitle() {
		return driver.getTitle();
	}

	public String getSource() {
		return driver.getPageSource();
	}

	public static WebDriver getDriver() {
		return new FirefoxDriver();
	}

	public List<String> getTextsFromListElements(List<WebElement> webElements) {
		List<String> result = new ArrayList<String>();
		for (WebElement webElement : webElements) {
			result.add(webElement.getText().replace("\n", "::").replace("\r", "::"));
		}
		return result;
	}
}
